import paho.mqtt.client as mqtt

# This is the Subscriber

def on_connect(client, userdata, flags, rc): #defines on_connect with the perameters client, userdata, flags, rc.
  print("Connected with result code "+str(rc)) # Once connected prints "connected with resut code" plus string of rc (return code), which returns 0.
  client.subscribe("Reuben/Networking") # Tells the client where to subscribe to

def on_message(client, userdata, msg): # defines on_message with the perameters client, userdata, msg.
  if msg.payload.decode() == "Herro": # says that if the payload message is "Herro" then...
    print("Success!") # print "Success"
    client.disconnect() # disconnects
    
client = mqtt.Client()  # Variable called client with the mqtt.Client 
client.connect("192.168.8.64",1883,60) # client connects to the broker IP, with the port number and keep alive interval

client.on_connect = on_connect # instantiates on connect
client.on_message = on_message # instantiates on message

client.loop_forever() # Program will loop forever until it gets the message
