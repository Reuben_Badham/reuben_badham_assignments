import paho.mqtt.client as mqtt

# This is the Publisher

client = mqtt.Client() 
client.connect("192.168.8.64",1883,60) # defines the broker ip, mosquito port, keep alive interval
client.publish("Reuben/Networking", "Herro"); # tells the publisher to publish to topic Reuben/Networking with the message "Herro"
client.disconnect(); # diconnects the client

