from digi.xbee.devices import XBeeDevice
import RPi.GPIO as GPIO
import time
REDLED = 16
xbee = XBeeDevice("/dev/ttyS0", 9600)
xbee.open()

def main():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(REDLED, GPIO.OUT)
    GPIO.output(REDLED, GPIO.LOW)
    print ("using pin%d for the RED LED"%REDLED)

xnet = xbee.get_network()

main()

xnet.start_discovery_process()
while xnet.is_discovery_running():
    time.sleep(1)

nodes = xnet.get_devices()
remote_device = xnet.discover_device(nodes[0]._node_id)
print(xnet.discover_device(nodes[0]._node_id))

try:
    while True:
        recv_msg = xbee.read_data_from(remote_device)
        if recv_msg != None:
            recv_msg = recv_msg.data.decode()
            if recv_msg == "Turn LED on":
                GPIO.output(REDLED, GPIO.HIGH)
                xbee.send_data(remote_device, "The Red LED is on")
            elif recv_msg == "Turn LED off":
                GPIO.output(REDLED, GPIO.LOW)
                xbee.send_data(remote_device, "The Red LED is off")

except KeyboardInterrupt:
    GPIO.cleanuo()
